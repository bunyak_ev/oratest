create or replace procedure adm.pr_login
is
begin
  insert into login (dt) values (sysdate);
  commit;
exception
  when others then
    rollback;
    raise;
end;
/